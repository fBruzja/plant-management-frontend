import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlantCreateComponent } from './plants/plant-create/plant-create.component';
import { PlantEditComponent } from './plants/plant-edit/plant-edit.component';
import { PlantOverviewComponent } from './plants/plant-overview/plant-overview.component';
import { NotFoundPageComponent } from './shared/component/not-found-page/not-found-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'plant',
    pathMatch: 'full'
  },
  {
    path: 'plant',
    component: PlantOverviewComponent,
  },
  {
    path: 'plant/add',
    component: PlantCreateComponent,
  },
  {
    path: 'plant/:id',
    component: PlantEditComponent,
  },
  {
    path: '**',
    component: NotFoundPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
