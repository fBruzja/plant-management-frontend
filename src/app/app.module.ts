import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './layout/main/main.component';
import { HeaderComponent } from './layout/header/header.component';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PlantListComponent } from './plants/plant-list/plant-list.component';
import { PlantOverviewComponent } from './plants/plant-overview/plant-overview.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { SharedModule } from './shared/shared.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ExpandableComponent } from './ui/expandable/expandable.component';
import { PlantCreateComponent } from './plants/plant-create/plant-create.component';
import { PlantFormComponent } from './plants/plant-form/plant-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { InputComponent } from './ui/input/input.component';
import { MatInputModule } from '@angular/material/input';
import { CheckboxComponent } from './ui/checkbox/checkbox.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PlantEditComponent } from './plants/plant-edit/plant-edit.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MainComponent,
    PlantOverviewComponent,
    PlantListComponent,
    ToolbarComponent,
    HeaderComponent,
    ExpandableComponent,
    PlantCreateComponent,
    PlantFormComponent,
    InputComponent,
    CheckboxComponent,
    PlantEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    SharedModule,
    RouterModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [MainComponent],
})
export class AppModule {}
