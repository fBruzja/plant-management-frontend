import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatatableValueTransformerPipe } from './pipe/datatable-value-transformer.pipe';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { OverlayComponent } from './component/overlay/overlay.component';
import { NotFoundPageComponent } from './component/not-found-page/not-found-page.component';


const PIPES = [DatatableValueTransformerPipe];

@NgModule({
  declarations: [ ...PIPES, OverlayComponent, NotFoundPageComponent],
  imports: [CommonModule, MatProgressSpinnerModule],
  exports: [...PIPES, OverlayComponent],
})
export class SharedModule {}
