import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PlantsService, PlantView } from '@fbruzja/plant-api';
import { UIMode } from 'src/app/ui/model/ui-mode';

export interface PlantFormData {
  name: string;
  indoor: boolean;
  timesToWater: string;
}

@Component({
  selector: 'app-plant-create',
  templateUrl: './plant-create.component.html',
  styleUrls: ['./plant-create.component.scss'],
})
export class PlantCreateComponent {
  loading: boolean = false;
  plantFormData: PlantFormData = { name: '', indoor: false, timesToWater: '0' };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private plantService: PlantsService
  ) {}

  onPlantDataChanged(data: PlantFormData) {
    this.plantFormData = data;
    this.createPlant();
  }

  createPlant(): void {
    if (this.plantFormData) {
      this.loading = true;
      const { name, indoor, timesToWater } = this.plantFormData;

      this.plantService
        .addPlant({
          name,
          indoor,
          timesToWater,
        })
        .subscribe((result: PlantView) => {
          this.router
            .navigate(['plant',result.id], { relativeTo: this.route.parent, queryParams: {mode: UIMode.edit} })
            .finally(() => {
              this.loading = false;
            });
        });
    }
  }
}
