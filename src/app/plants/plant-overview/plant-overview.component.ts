import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlantsService, PlantView } from '@fbruzja/plant-api';
import { UIMode } from 'src/app/ui/model/ui-mode';

@Component({
  selector: 'app-plant-overview',
  templateUrl: './plant-overview.component.html',
  styleUrls: ['./plant-overview.component.scss'],
})
export class PlantOverviewComponent implements OnInit {
  plants: PlantView[] = [];
  rows: PlantView[] = [];
  loading = false;

  constructor(
    private router: Router,
    private plantService: PlantsService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.plantService.getPlantList().subscribe((result: PlantView[]) => {
      this.plants = result;
      this.rows = result;
      this.loading = false;
    });
  }

  async onPlantSelected(plantId: string): Promise<void> {
    await this.router.navigate([plantId], {
      relativeTo: this.route,
      queryParams: { mode: UIMode.detail },
    });
  }
}
