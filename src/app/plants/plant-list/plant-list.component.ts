import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PlantView } from '@fbruzja/plant-api';
import { DatatableValueTransformerPipe } from 'src/app/shared/pipe/datatable-value-transformer.pipe';

export interface ColumnConfig {
  name?: boolean;
  timesToWater?: boolean;
  indoor?: boolean;
}

@Component({
  selector: 'app-plant-list',
  templateUrl: './plant-list.component.html',
  styleUrls: ['./plant-list.component.scss'],
})
export class PlantListComponent implements OnInit {
  valueTransformerPipe = new DatatableValueTransformerPipe();

  @Output() rowClicked: EventEmitter<string> = new EventEmitter<string>();

  @Input()
  internalColumnConfig: ColumnConfig = {
    name: true,
    timesToWater: true,
    indoor: true,
  };

  @Input()
  plants: PlantView[] = [];

  @Input()
  set columnConfig(config: ColumnConfig) {
    if (config) {
      Object.assign(this.internalColumnConfig, config);
    }
  }

  ngOnInit(): void {}

  onRowClick(plantId: string) {
    this.rowClicked.emit(plantId);
  }
}
