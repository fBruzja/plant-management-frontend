import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PlantFormData } from '../plant-create/plant-create.component';

@Component({
  selector: 'app-plant-form',
  templateUrl: './plant-form.component.html',
  styleUrls: ['./plant-form.component.scss'],
})
export class PlantFormComponent implements OnInit {
  @Input()
  set formData(value: PlantFormData) {
    if (value) {
      this.formGroup.patchValue(
        {
          name: value.name,
          indoor: value.indoor,
          timesToWater: value.timesToWater,
        },
        { emitEvent: false }
      );
    }
  }

  @Input()
  set disabled(disabled: boolean) {
    if (disabled) {
      this.formGroup.disable({ emitEvent: false });
    } else {
      this.formGroup.enable({ emitEvent: false });
    }
  }

  @Input()
  isIndoor = false;

  @Output()
  plantChanged: EventEmitter<PlantFormData> = new EventEmitter<PlantFormData>();

  formGroup = new FormGroup(
    {
      name: new FormControl('', Validators.required),
      timesToWater: new FormControl('', Validators.required),
      indoor: new FormControl(false),
    },
    {
      updateOn: 'blur',
    }
  );

  constructor() {}

  ngOnInit(): void {
    this.formGroup.valueChanges.subscribe((formData) => {
      if (this.formGroup.valid) {
        this.plantChanged.emit(formData as PlantFormData);
      }
    });
  }
}
