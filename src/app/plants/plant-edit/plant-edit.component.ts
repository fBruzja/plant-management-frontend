import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlantsService, PlantView, UpdatePlantDto } from '@fbruzja/plant-api';
import { UIMode } from 'src/app/ui/model/ui-mode';
import { PlantFormData } from '../plant-create/plant-create.component';

@Component({
  selector: 'app-plant-edit',
  templateUrl: './plant-edit.component.html',
  styleUrls: ['./plant-edit.component.scss'],
})
export class PlantEditComponent implements OnInit {
  disabled = true;
  loading = false;
  plant: PlantView = {};
  plantFormData: PlantFormData = { name: '', indoor: false, timesToWater: '0' };

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.loadPlant(params.get('id')!);
    });
    this.route.queryParams.subscribe((query: { mode?: UIMode }) => {
      if (query.mode === UIMode.edit) {
        this.disabled = false;
      }
    });
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private plantService: PlantsService
  ) {}

  loadPlant(plantId: string) {
    this.loading = true;
    this.plantService.getPlantById(plantId).subscribe((plant: PlantView) => {
      this.plant = plant;
      this.loading = false;
    });
  }

  enableEdit() {
    this.disabled = false;
  }

  onPlantDataChanged(data: PlantFormData) {
    this.plantFormData = data;
    this.updatePlant();
  }

  updatePlant() {
    const dto: UpdatePlantDto = {
      name: this.plantFormData.name,
      indoor: this.plantFormData.indoor,
      timesToWater: this.plantFormData.timesToWater,
    };

    this.plantService.updatePlant(this.plant.id!, dto).subscribe(() => {
      this.reloadPlant();
    });
  }

  onPlantDeleteClick() {
    this.plantService.removePlant(this.plant.id!).subscribe({
      next: async () => {
        await this.router.navigate(['/plant']);
      },
      error: (err) => {
        console.error('Error Deleting => ', err);
      },
    });
  }

  reloadPlant() {
    this.loadPlant(this.plant.id!);
  }
}
